﻿using System;
using System.Collections.Generic;

namespace agework
{
    class Program
    { 
        static void Main(string[] args)
        {
            //number stuff
            int e = 19;
            int q = 18;
            Console.WriteLine(q);
            double a = 1.0;//example of double usage
            Console.WriteLine(a);
            a = 1.0 / 0.4;//example of the double working
            Console.WriteLine(a);
            List<double> AgeList = new List<double>();
            AgeList.Add(a);
            AgeList.Add(q);
            AgeList.Add(e);
            Console.WriteLine(AgeList);//This is meant to show what a list is need fix
            //this is c# varibles string and int
            string name = "John";
            Console.WriteLine(name);
            int myNum;
            myNum = 15;//this varible has been set to the whole number 15 myNum is now 15
            Console.WriteLine(myNum);
            //const int myNum = 15; - This is to constantly make the number 15 nothing can overwrite
            //myNum = 20; - This would cause an error as myNum cant be changed
            //bool -tru/fal
            long w = 9223372036854775807;//example of long usage no more than this number isnt allowed
            Console.WriteLine(w);
            long y = 9223372036854775806;//example of long usage no more than this number isnt allowed
            Console.WriteLine(y);
            y = w + y;
            Console.WriteLine(y);//shows how it doesnt work
            bool myBool = true;
            bool notmyBool = false;
            Console.WriteLine(myBool);   // Outputs True
            Console.WriteLine(notmyBool);   // Outputs False
            char sites = 'O';
            Console.WriteLine(sites);//can only store 1 character




        }
    }
}
